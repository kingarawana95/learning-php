<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	<title>Form Feedback</title>
</head>
<body>
	<?php # Script 2.3 - handle_form.php
	
	// shortcuts to form data
	$name = $_REQUEST['name'];
	$email = $_REQUEST['email'];
	$comments = stripslashes($_REQUEST['comments']);
	
	if(isset($_REQUEST['gender'])) {
		$gender = $_REQUEST['gender'];
	} else {
		$gender = NULL;
	}
	/* Not used:
	$_REQUEST['age']
	$_REQUEST['submit']
	*/
	
	if($gender == "M"){
		$prefix = "Sir";
	} elseif($gender == "F"){
		$prefix = "Madam";
	} else {
		$prefix = "I'm-not-sure-what-you-are";
	}
	
	// Print info
	echo "<p>Thank you, <b>$prefix $name</b>, for the following comments: <br />
		<tt>$comments</tt></p>
		
		<p>We will reply to you at <i>$email</i>.</p>\n";
	?>
</body>
</html>