<?php # Script 9.3 - edit_user.php

// This page is for editing a user record.

$page_title = 'Edit a User';
include('includes/header.html');

echo '<h1>Edit a User</h1>';

// Check for a valid user ID, through GET or POST:
if ((isset($_GET['id'])) && (is_numeric($_GET['id']))) {
	$id = $_GET['id'];
} elseif ((isset($_POST['id'])) && (is_numeric($_POST['id']))) {
	$id = $_POST['id'];
} else {
	echo '<p class="error">This page has been accessed in error.</p>';
	include ('includes/footer.html');
	exit();
}

require_once ('includes/mysqli_connect.php');

// Check if the form has been submitted:
if (isset($_POST['submitted'])) {
	$errors = array();
	
	// Check for a first name:
	if (empty($_POST['first_name'])) {
		$errors[] = 'You forgot to enter your first name.';
	} else {
		$fn = mysqli_real_escape_string($dbc, trim($_POST['first_name']));
	}
	
	// Check for the last name:
	if (empty($_POST['last_name'])) {
		$errors[] = 'You forgot to enter your last name.';
	} else {
		$ln = mysqli_real_escape_string($dbc, trim($_POST['last_name']));
	}
	
	// Check for an email address:
	if (empty($_POST['email'])) {
		$errors[] = 'You forgot to enter your email address.';
	} else {
		$e = mysqli_real_escape_string($dbc, trim($_POST['email']));
	}
	
	if (empty($errors)) { // If there are no errors
		// Test for unique email address
		$q = "select user_id from users where email='$e' and user_id != $id";
		$r = @mysqli_query($dbc, $q);
		
		if (mysqli_num_rows($r) == 0) {
			// Make the query
			$q = "update users set first_name='$fn', last_name='$ln', email='$e' where user_id=$id limit 1";
			$r = @mysqli_query ($dbc, $q);
			if (mysqli_affected_rows($dbc) == 1) {
				// Print a message:
				echo '<p>The user has been edited.</p>';
			} else {
				echo '<p class="error">The user could not be edited due to a system error.</p>'; // Public message
				echo '<p>' . mysqli_error($dbc) . '<br />Query: ' . $q . '</p>'; // Debugging message
			}
		} else {
			echo '<p class="error">The email address has already been registered.</p>';
		}
	} else {
		echo '<p class="error">The following error(s) occurred:<br />';
		foreach ($errors as $msg) {
			echo " - $msg<br />\n";
		}
		
		echo '</p><p>Please try again.</p>';
	}
}

// Always show the form

/*
$q = "select first_name, last_name, email from users where user_id=?";
$stmt = mysqli_prepare($dbc, $q);
$r = mysqli_stmt_bind_param($stmt, 'i', $id);
*/
$q = "select first_name, last_name, email from users where user_id=$id";
$r = @mysqli_query($dbc, $q);

if (mysqli_num_rows($r) == 1) {
	// Get the users' information:
	$row = mysqli_fetch_array ($r, MYSQLI_NUM);
	// Create the form:
	echo '<form action="edit_user.php" method="post">
	<p>First Name: <input type="text" name="first_name" size="15" maxlength="15" value="' . $row[0] . '" /></p>
	<p>Last Name: <input type="text" name="last_name" size="15" maxlength="30" value="' . $row[1] . '" /></p>
	<p>Email Address: <input type="text" name="email" size="20" maxlength="40" value="' . $row[2] . '" /></p>
	<p><input type="image" name="submit" src="http://contact.bellsouth.com/subpoena/images/btn_update_entry.gif" value="Submit" /></p>
	<input type="hidden" name="submitted" value="TRUE" />
	<input type="hidden" name="id" value="' . $id . '" />
	</form>';
} else {
	echo '<p class="error">This page has been accessed in error.</p>';
}

mysqli_close($dbc);
include('includes/footer.html');
?>