<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	<title>Concatenation</title>
</head>
<body>
	<?php # Script 1.7 - concat.php
	
	// Creating the variables
	$first_name = 'Gary';
	$last_name = 'Shteyngart';
	$author = $first_name . ' ' . $last_name;
	$book = 'Super Sad True Love Story';
	
	//Print the values
	echo "<p>The book <em>$book</em> was written by $author.</p>";
	
	// How long is Gary's name?
	$num = strlen($author);
	echo "How long is the author's name? <br />$num characters";
	?>
</body>
</html>