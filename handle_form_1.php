<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	<title>Form Feedback</title>
	<style type="text/css" title="text/css" media="all">
	.error{
		font-weight: bold;
		color: #C00
	}
	</style>
</head>
<body>
	<?php # Script 2.4 - handle_form.php
	
	//validate name
	if(!empty($_REQUEST['name'])) {
		$name = $_REQUEST['name'];
	} else {
		$name = NULL;
		echo '<p class="error">You forgot to enter your name!</p>';
	}
	
	// validate email
	if (!empty($_REQUEST['email'])) {
		$email = $_REQUEST['email'];
	} else {
		$email = NULL;
		echo '<p class="error">You forgot to enter your email!</p>';
	}
	
	// validate comments
	if (!empty($_REQUEST['comments'])) {
		$comments = $_REQUEST['comments'];
	} else {
		$comments = NULL;
		echo '<p class="error">You forgot to enter your comments!</p>';
	}
	
	// validate gender
	if (isset($_REQUEST['gender'])){
		$gender = $_REQUEST['gender'];
		if($gender == "M"){
			$prefix = "Sir";
		} elseif($gender == "F"){
			$prefix = "Madam";
		} else {
			$prefix = "I'm-not-sure-what-you-are";
			echo '<p class="error">Gender must be M or F.</p>';
		}
	} else {
		$gender = NULL;
		$prefix = "I'm-not-sure-what-you-are";
		echo '<p class="error">You must enter your gender.</p>';
	}
	if ($name && $email && $gender && $comments){
		// Print info
		echo "<p>Thank you, <b>$prefix $name</b>, for the following comments: <br />
			<tt>$comments</tt></p>
			
			<p>We will reply to you at <i>$email</i>.</p>\n";
	} else {
		echo '<p class="error">Please go back and fill in the form again.</p>';
	}
	?>
</body>
</html>