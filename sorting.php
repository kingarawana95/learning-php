<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	<title>Sorting Arrays</title>
	<style type="text/css" title="text/css" media="all">
	.error{
		font-weight: bold;
		color: #C00
	}
	</style>
</head>
<body>
	<p><h1>MOVIES!</h1></p>
	<table border="0" cellspacing="3" cellpadding="3" align="center">
		<tr>
			<td><h2>Rating</h2></td>
			<td><h2>Title</h2></td>
		</tr>
		<?php # Script 2.8 - sorting.php
		// Create the array
		$movies = array(
		10 => 'Trainspotting',
		2 => 'Donnie Darko',
		5 => 'The Batchelor',
		9 => 'Pennsylvania Story',
		1 => 'The Hobbit');
		
		// Display movies in original order
		echo '<tr><td colspan="2"><b>In their original order:</b></td></tr>';
		foreach ($movies as $k => $v){
			echo "<tr><td>$k</td>
				<td>$v</td></tr>\n";
		}
		
		//Display movies sorted by title
		asort($movies);
		echo '<tr><td colspan="2"><b>Sorted by title:</b></td></tr>';
		foreach($movies as $k => $v){
			echo "<tr><td>$k</td>
			<td>$v</td></tr>\n";
		}
		
		//Display movies sorted by rating
		krsort($movies);
		echo '<tr><td colspan="2"><b>Sorted by rating:</b></td></tr>';
		foreach($movies as $k => $v){
			echo "<tr><td>$k</td>
			<td>$v</td></tr>\n";
		}
		?>
	</table>
</body>
</html>