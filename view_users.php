<?php # Script 9.5 - view_users.php #5

// Retrieves all the records from the users table
// This new version links to edit and delete pages.

$page_title = 'View the Current Users';
include('includes/header.html');
echo '<h1>Registered Users</h1>';

require_once('includes/mysqli_connect.php');

// Number of records to show per page:
$display = 10;

// Determine how many pages there are...
if (isset($_GET['p']) && is_numeric($_GET['p'])) {
	$pages = $_GET['p'];
} else {
	// Count the number of records:
	$q = "select count(user_id) from users";
	$r = @mysqli_query ($dbc, $q);
	$row = @mysqli_fetch_array ($r, MYSQLI_NUM);
	$records = $row[0];
	
	// Calculate number of pages
	if ($records > $display) {
		$pages = ceil ($records/$display);
	} else {
		$pages = 1;
	}
}

// Determine where in the database to start returning results
if (isset($_GET['s']) && is_numeric($_GET['s'])) {
	$start = $_GET['s'];
} else {
	$start = 0;
}

// Determine the sort
$sort = (isset($_GET['sort'])) ? $_GET['sort'] : 'rd';

// Determine the sorting order
switch ($sort) {
	case 'ln':
		$order_by = 'last_name asc';
		break;
	case 'fn':
		$order_by = 'first_name asc';
		break;
	case 'rd':
		$order_by = 'registration_date asc';
		break;
	default:
		$order_by = 'registration_date asc';
		$sort = 'rd';
		break;
}
// Make the query
$q = "select last_name, first_name, date_format(registration_date, '%M %d, %Y') as dr, user_id from users order by $order_by limit $start, $display";
$r = @mysqli_query ($dbc, $q);

// Table header:
echo '<table align="center" cellspacing="0" cellpadding="5" width="75%">
<tr>
<td align="left"><b>Edit</b></td>
<td align="left"><b>Delete</b></td>
<td align="left"><b><a href="view_users.php?sort=ln">Last Name</a></b></td>
<td align="left"><b><a href="view_users.php?sort=fn">First Name</a></b></td>
<td align="left"><b><a href="view_users.php?sort=dr">Date Registered</a></b></td>
</tr>';

// Fetch and print all the records
$bg = '#eeeeee'; // Set the initial background color
while ($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {
	$bg = ($bg=='#bfd4f0' ? '#dfebff' : '#bfd4f0'); // Switch the background color
	
	echo '<tr bgcolor="' . $bg . '">
	<td align="left"><a href="edit_user.php?id=' . $row['user_id'] . '"><img src="http://s1.runnersworld.co.uk/forum/images/edit.png"></a></td>
	<td align="left"><a href="delete_user.php?id=' . $row['user_id'] . '"><img src="http://fundacionmesco.org/sistema/ventas/menu/common/stylesheet/mozilla/image/button-delete.png"></a></td>
	<td align="left">' . $row['last_name'] . '</td>
	<td align="left">' . $row['first_name'] . '</td>
	<td align="left">' . $row['dr'] . '</td>
	</tr>';
}

echo '</table>';
mysqli_free_result ($r);
mysqli_close($dbc);

// Make the links to other pages, if necessary
if ($pages > 1) {
	// Add some spacing and start a paragraph:
	echo '<br /><p>';
	
	// Determine what page the script is on:
	$current_page = ($start/$display) + 1;
	
	//If it's not eh first page, make a previous button:
	if ($current_page != 1) {
		echo '<a href="view_users.php?s=' . ($start - $display) . '&p=' . $pages . '&sort=' . $sort . '">Previous</a> ';
	}
	
	// Make all the numbered pages:
	for ($i = 1; $i <= $pages; $i++) {
		if ($i != $current_page) {
			echo '<a href="view_users.php?s=' . (($display * ($i -1))) . '&p=' . $pages . '&sort=' . $sort . '">' . $i . '</a> ';
		} else {
			echo $i . ' ';
		}
	}
	
	// If it's not the last page, make a next button
	if ($current_page != $pages) {
		echo '<a href="view_users.php?s=' . ($start + $display) . '&p=' . $pages . '&sort=' . $sort . '">Next</a>';
	}
	
	echo '</p>';
}
include('includes/footer.html');
?>