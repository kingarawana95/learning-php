create database forum;
use forum;
create table forums (forum_id tinyint unsigned not null auto_increment, name varchar(60) not null, primary key (forum_id));

create table messages ( message_id int unsigned not null auto_increment, forum_id tinyint unsigned not null, parent_id int unsigned not null, user_id mediumint unsigned not null, subject varchar(100) not null, body longtext not null, date_entered timestamp not null, primary key (message_id));

create table users ( user_id mediumint unsigned not null auto_increment, username varchar(30) not null, pass char(40) not null, first_name varchar(20) not null, last_name varchar(40) not null, email varchar(80) not null, primary key (user_id));

insert into forums (name) values ('MySQL'),('PHP'),('Ruby'),('HTML'),('CSS');

insert into users (username, pass, first_name, last_name, email) values ('jmorton429', SHA1('mypass'), 'Jennifer', 'Morton', 'jen@example.com'), ('admiral', SHA1('bsg'), 'Bill', 'Adama', 'admiral@gmail.com'), ('starbuck', SHA1('apollo'), 'Kara', 'Thrace', 'starbuck@gmail.com');

insert into messages (forum_id, parent_id, user_id, subject, body) values (1,0,1,'Differences between SQL and mySQL?', 'I still don\'t get it... anyone want to help me out?'), (2,0,3,'What is PHP anyway?','I just want to fly. Why are you making me learn to code?'), (2,3,2,'What is PHP anyway?','You need to educate yourself Starbuck!');

select * from messages inner join forums on messages.forum_id = forums.forum_id where forums.name = 'MySQL';
select * from messages inner join forums using (forum_id);
select * from forums left join messages using (forum_id);
select m.subject, date_format(m.date_entered, '%M %D, %y') as date from users as u inner join messages as m using (user_id) where u.username = 'jmorton429';

INSERT INTO messages VALUES(4, 1, 0, 1, 'Question about normalization.', 'I''m confused about normalization. For the second normal form (2NF), I read...', NOW());
INSERT INTO messages (forum_id, parent_id, user_id, subject, body) VALUES(1, 0, 3, 'Database Design', 'I''m creating a new database and am having problems with the structure. How many tables should I have?...');
INSERT INTO messages (forum_id, parent_id, user_id, subject, body) VALUES(1, 5, 2, 'Database Design', 'The number of tables your database includes...');
INSERT INTO messages (forum_id, parent_id, user_id, subject, body) VALUES(1, 5, 3, 'Database Design', 'Okay, thanks!');
INSERT INTO messages (forum_id, parent_id, user_id, subject, body) VALUES(2, 0, 3, 'PHP Errors', 'I''m using the scripts from Chapter 3 and I can''t get the first calculator example to work. When I submit the form...');
INSERT INTO messages (forum_id, parent_id, user_id, subject, body) VALUES(2, 8, 1, 'PHP Errors', 'What version of PHP are you using?');
INSERT INTO messages (forum_id, parent_id, user_id, subject, body) VALUES(2, 8, 3, 'PHP Errors', 'Version 5.2');
INSERT INTO messages (forum_id, parent_id, user_id, subject, body) VALUES(2, 8, 1, 'PHP Errors', 'In that case, try the debugging steps outlined in Chapter 7.');

select username, count(message_id) as number from users as u left join messages as m using (user_id) group by (u.user_id);

alter table forums add unique(name);
alter table messages engine = myisam;
alter table messages add index(forum_id), add index(parent_id), add index(user_id), add fulltext(body, subject), add index(date_entered);
alter table users add unique(username), add index(pass,username), add unique(email);

select subject, body from messages where match (body,subject) against('database');
select subject, body, match (body, subject) against('database') as r from messages where match (body, subject) against('database');
select subject, body from messages where match(body, subject) against('data*' in boolean mode)\G
select subject, body from messages where match(body, subject) against('>"normal form"* + database*' in boolean mode)\G


/* transactions */

create database test;
use test;
insert into accounts (name, balance) values ('Sarah Vowell', 5460.23), ('David Sedaris', 909325.24), ('Kojo Nnamdi', 892.00);
start transaction;
update accounts set balance = (balance-100) where id=2;
update accounts set balance = (balance+100) where id=1;
commit;