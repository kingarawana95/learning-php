<?php # Script 13.1 - pcre.php

$page_title = 'Testing PCRE';

include ('includes/header.html');
include ('includes/report_errors.php');

if (isset($_POST['submitted'])) {
	
	// Trim the strings
	$pattern = trim($_POST['pattern']);
	$subject = trim($_POST['subject']);
	
	// Print a caption:
	echo "<p>The result of checking<br /><b>$pattern</b><br />against<br />$subject<br />is ";
	
	// Test:
	if (preg_match($pattern, $subject)) {
		echo 'TRUE!</p>';
	} else {
		echo 'FALSE!</p>';
	}
}
?>
<form action="pcre.php" method="post">
	<p>Regular Expression Pattern: <input type="text" name="pattern" value="<?php if (isset($pattern)) echo $pattern; ?>" size="30" /></p>
	<p>Test Subject: <input type="text" name="subject" value="<?php if (isset($subject)) echo $subject; ?>" size="30" /></p>
	<input type="submit" name="submit" value="Test!" />
	<input type="hidden" name="submitted" value="TRUE" />
</form>
<?php
include ('includes/footer.html');
?>