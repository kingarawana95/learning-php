<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	<title>Calendar</title>
	<style type="text/css" title="text/css" media="all">
	.error{
		font-weight: bold;
		color: #C00
	}
	</style>
</head>
<body>
	<form action="calendar.php" method="post">	
	<?php # Script 2.9 - calendar.php
	// This makes three pull-down menus
	
	// Make months array:
	$months = array (1 => 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
	
	// Make the days and years arrays:
	// $days = range (1, 31);
	// $years = range (2013, 2023);
	
	// Make months a pull-down menu:
	echo '<select name="month">';
	foreach($months as $k => $v){
		echo "<option value=\"$k\">$v</option>\n";
	}
		
	echo '</select>';
		
	// Make days a pull-down menu:
	echo '<select name="day">';
	for ($day = 1; $day <= 31; $day++) {
		echo "<option value=\"$day\">$day</option>\n";
	}
	echo '</select>';
	
	// Make the years a pull-down menu:
	echo '<select name="year">';
	for ($year = 2013; $year <= 2023; $year ++) {
		echo "<option value=\"$year\">$year</option>\n";
	}
	echo '</select>';
	
	?>
	</form>
</body>
</html>