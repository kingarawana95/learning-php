<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	<title>Numbers</title>
</head>
<body>
	<?php # Script 1.8 - numbers.php
	
	// Set the variables
	$quantity = 30;
	$price = 119.95;
	$discount = .20;
	$taxrate = .05;
	
	// Calculate the total
	$total = $quantity * $price;
	$total = $total - ($total * $discount);
	$total2 = $total + ($total * $taxrate); // Calculate tax rate
	
	// Format total
	$total = number_format($total, 2);
	
	// Print the results
	echo '<p>You are purchasing <b>' . $quantity . '</b> widget(s) at a cost of <b>$' . $price . '</b> each. With the 20% discount, the price is <b>$' . $total . '</b>. With tax, the total comes to <b>$' . $total2 . '</b>.</p>'; 
	?>
</body>
</html>