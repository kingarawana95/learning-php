<?php # Script 11.1 - login_page.inc.php
// This page prints any errors associated with logging in

// Include the header:
$page_title = 'Login';
include('includes/header.html');

// Print any error messages, if they exist:
if (!empty($errors)) {
	echo '<h1>Error!</h1>
	<p class="error">The following error(s) occurred:<br />';
	foreach ($errors as $msg) {
		echo " - $msg<br />\n";
	}
	echo '</p><p>Please try again.</p>';
}

// Display the form
?>
<h1>Login</h1>

<form action="login.php" method="post">
	<p>Email Address: <input type="text" name="email" size="20" maxlength="80" /></p>
	<p>Password: <input type="password" name="pass" size="20" maxlength="20" /></p>
	<p><input name="submit" type="image" src="http://www.xinje-support-centre-listo.com/images/button_login.png" value="Login" /></p>
	<input type="hidden" name="submitted" value="TRUE" />
</form>

<?php // Include the footer:
include('includes/footer.html');
?>