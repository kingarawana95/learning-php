<?php # Script 7.1 - display_errors.php
	// Show errors:
/* 	ini_set('display_errors', 1); */
	
	// Adjust error reporting
/* 	error_reporting(E_ALL); */

// Flag variable for site status:
define('LIVE', FALSE);

// Create the error handler:
function my_error_handler ($e_number, $e_message, $e_file, $e_line, $e_vars) {
	// Build the error message
	$message = "An error occurred in script '$e_file' on line $e_line: $e_message\n";
	
	// Append $e_vars to $message:
	$message .= print_r($e_vars, 1);
	
	if (!LIVE) { // Development (print the error)
		echo '<pre>' . $message . "\n";
		debug_print_backtrace();
		echo '</pre><br />';
	} else {
		echo '<div class="error">A system error occurred. We apologize for the inconvenience.</div><br />';
	}
}
set_error_handler('my_error_handler');

?>