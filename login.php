<?php # Script 11.12 - login.php #4

// this page processes the login form submission
// Upon successful login, the user is redirected.
// Two included files are necessary.
// Send nothing to the web browser prior to the setcookie() lines

// Check if the form has been submitted
if (isset($_POST['submitted'])) {
	// For processing the login
	require_once('includes/login_functions.inc.php');
	
	// Need the database connection
	require_once('includes/mysqli_connect.php');
	
	// Check the login
	list($check, $data) = check_login($dbc, $_POST['email'], $_POST['pass']);
	if ($check) {
		// Set the session data
		session_start();
		$_SESSION['user_id'] = $data['user_id'];
		$_SESSION['first_name'] = $data['first_name'];
		
		// Store the HTTP_USER_AGENT
		$_SESSION['agent'] = md5($_SERVER['HTTP_USER_AGENT']);
		
		// Redirect
		$url = absolute_url ('loggedin.php');
		header("Location: $url");
		exit(); // Quit the script
	} else {
		// Assign $data to $errors for error reporting
		$errors = $data;
	}
	mysqli_close($dbc); // Close the database connection
}

// Create the page
include('includes/login_page.inc.php');
?>