mysql> use test;
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
mysql> create table encode ( id int unsigned not null auto_increment, card_number tinyblob, primary key (id));
Query OK, 0 rows affected (0.17 sec)

mysql> insert into encode (id, card_number) values (null, aes_encrypt(1234567890123456,'eLL10tT'));
Query OK, 1 row affected (0.19 sec)

mysql> select id, aes_decrypt(card_number, 'eLL10tT') as cc from encode;
+----+------------------+
| id | cc               |
+----+------------------+
|  1 | 1234567890123456 |
+----+------------------+
1 row in set (0.00 sec)

mysql> select * from encode;
+----+----------------------------------+
| id | card_number                      |
+----+----------------------------------+
|  1 | �b@�_h4�/q��(��y�B�ȴs/鉖�t�*�               |
+----+----------------------------------+
1 row in set (0.00 sec)

mysql> truncate table encode;
Query OK, 0 rows affected (0.14 sec)

mysql> create table aes_salt ( salt varchar(12) not null );
Query OK, 0 rows affected (0.09 sec)

mysql> insert into aes_salt (salt) values ('0bfuscate');
Query OK, 1 row affected (0.01 sec)

mysql> select @salt:=salt from aes_salt;
+-------------+
| @salt:=salt |
+-------------+
| 0bfuscate   |
+-------------+
1 row in set (0.06 sec)

mysql> insert into encode (id, card_number) values (null, aes_encrypt(1234567890123456,@salt));
Query OK, 1 row affected (0.00 sec)

mysql> select @salt:salt from aes_salt;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ':salt from aes_salt' at line 1
mysql> select @salt:=salt from aes_salt;
+-------------+
| @salt:=salt |
+-------------+
| 0bfuscate   |
+-------------+
1 row in set (0.00 sec)

mysql> select id, aes_decrypt(card_number,@salt) as cc from encode;
+----+------------------+
| id | cc               |
+----+------------------+
|  1 | 1234567890123456 |
+----+------------------+
1 row in set (0.00 sec)

mysql>