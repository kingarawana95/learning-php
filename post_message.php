<?php # Script 12.4 - post_message.php

$page_title = 'Post a Message';

include ('includes/header.html');
include ('includes/report_errors.php');

if (isset($_POST['submitted'])) {
	// Validate the data
	// Connect to the database
	$dbc = mysqli_connect('localhost', 'username', 'password', 'forum');
	
	// Make the query
	$q = 'insert into messages (forum_id, parent_id, user_id, subject, body, date_entered) values (?, ?, ?, ?, ?, now())';
	
	// prepare the statement
	$stmt = mysqli_prepare($dbc, $q);
	
	// Bind the variables
	// This statement is still not working
	mysqli_stmt_bind_param($stmt, 'iiiss', $forum_id, $parent_id, $user_id, $subject, $body);
	
	// Assign the values to variables:
	$forum_id = (int) $_POST['forum_id'];
	$parent_id = (int) $_POST['parent_id'];
	
	$user_id = $_SESSION['user_id']; // The user_id value would normally come from the session.
	$subject = strip_tags($_POST['subject']);
	$body = strip_tags($_POST['body']);
	
	// Execute the query
	mysqli_stmt_execute($stmt);
	
	// Print a message based upon the result
	if (mysqli_stmt_affected_rows($stmt) == 1) {
		echo '<p>Your message has been posted.</p>';
	} else {
		echo '<p style="font-weight: bold; color: #C00">Your message could not be posted.</p>';
		echo '<p>' . mysqli_stmt_error($stmt) . '</p>';
	}
	
	// Close the statement
	mysqli_stmt_close($stmt);
	
	// Close the connection
	mysqli_close($dbc);
}
?>
<form action="post_message.php" method="post">
	<fieldset><legend>Post a message: </legend>
		<p><b> Subject</b>: <input name="subject" type="text" size="30" maxlength="100" /></p>
		<p><b> Body</b>: <textarea name="body" rows="3" cols="40"></textarea></p>
	</fieldset>
	<div align="center"><input type="submit" name="submit" value="Submit" /></div>
	<input type="hidden" name="submitted" value="TRUE" />
	<input type="hidden" name="forum_id" value="1" />
	<input type="hidden" name="parent_id" value="0" />
</form>
<?php
include ('includes/footer.html');
?>