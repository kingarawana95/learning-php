<?php # Script 3.9 - calculator.php #4

$page_title = 'Widget Cost Calculator';
include('includes/header.html');

function calculate_total ($qty, $cost, $tax = 5) {
	$total = ($qty * $cost);
	$taxrate = ($tax /100);
	$total += ($total * $taxrate);
	
	echo '<p>The total cost of purchasing ' . $qty . ' widget(s) at $' . number_format($cost, 2) . ' each, including a tax rate of ' . $tax . '%, is $' . number_format($total, 2) . '.</p>';
}

// Check for form submission:
if (isset($_POST['submitted'])) {
	// Minimal form validation
	if (is_numeric($_POST['quantity']) && is_numeric($_POST['price'])) {
		// Print the results:
		echo '<h1>Total Cost:</h1>';
		if (is_numeric($_POST['tax'])){
			calculate_total($_POST['quantity'],$_POST['price'],$_POST['tax']);
		} else {
			calculate_total($_POST['quantity'],$_POST['price']);
		}
	} else { // Invalid submitted values
		echo '<h1>Error!</h1>
		<p class="error">Please enter a valid quantity, price, and tax.</p>';
	}
} // End of main isset() IF.
?>

<h1>Widget Cost Calculator</h1>
<form action="calculator.php" method="post">
	<p>Quantity: <input name="quantity" type="text" size="5" maxlength="5" value="<?php if (isset($_POST['quantity'])) echo $_POST['quantity']; ?>"/></p>
	<p>Price: <input name="price" type="text" size="5" maxlength="10"value="<?php if (isset($_POST['price'])) echo $_POST['price']; ?>"/></p>
	<p>Tax (%): <input name="tax" type="text" size="5" maxlength="5"value="<?php if (isset($_POST['tax'])) echo $_POST['tax']; ?>"/> (optional)</p>
	<p><input name="submit" type="image" src="http://www.legacyrealtyinc.com/images/siteButton-calculate-down.png" value="Calculate" /></p>
	<input name="submitted" type="hidden" value="1"/>
</form>

<?php // Include footer
include ('includes/footer.html');
?>