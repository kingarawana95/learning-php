<?php # Script 11.5 - login.php #2

// this page processes the login form submission
// Upon successful login, the user is redirected.
// Two included files are necessary.
// Send nothing to the web browser prior to the setcookie() lines

// Check if the form has been submitted
if (isset($_POST['submitted'])) {
	// For processing the login
	require_once('includes/login_functions.inc.php');
	
	// Need the database connection
	require_once('includes/mysqli_connect.php');
	
	// Check the login
	list($check, $data) = check_login($dbc, $_POST['email'], $_POST['pass']);
	if ($check) {
		// Set the cookies
		setcookie('user_id', $data['user_id'], time()+3600, '/', '', 0, 0);
		setcookie('first_name', $data['first_name'], time()+3600, '/', '', 0, 0);
		
		// Redirect
		$url = absolute_url ('loggedin.php');
		header("Location: $url");
		exit(); // Quit the script
	} else {
		// Assign $data to $errors for error reporting
		$errors = $data;
	}
	mysqli_close($dbc); // Close the database connection
}

// Create the page
include('includes/login_page.inc.php');
?>