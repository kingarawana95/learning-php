<?php # Script 3.7 - dateform.php

$page_title = 'Calendar Form';
include ('includes/header.html');

// This function makes pull down menus
function make_calendar_pulldowns() {
	// Months array
	$months = array (1 => 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
	
	// Make months a pull down menu
	echo '<select name="month">';
		
	foreach ($months as $key => $value) {
		echo "<option value=\"$key\">$value</option>\n";
	}
	
	echo '</select>';
	
	// Make the days pull-down menu:
	
	echo '<select name="day">';
	for ($day = 1; $day <=31; $day++) {
		echo "<option value=\"$day\">$day</option>\n";
	}
	echo '</select>';
	
	// Make the years pull-down
	echo '<select name="year">';
	for ($year = 2013; $year <=2023; $year++) {
		echo "<option value=\"$year\">$year</option>\n";
	}
	echo '</select>';
}

echo '<h1>Select a Date:</h1>
<br/>
<form action="dateform.php" method="post">';
	
make_calendar_pulldowns();

echo '</form>';

include('includes/footer.html');

?>