<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	<title>Strings</title>
</head>
<body>
	<?php # Script 1.6 - strings.php
	
	// Creating the variables
	$first_name = 'Gary';
	$last_name = 'Shteyngart';
	$book = 'Super Sad True Love Story';
	
	//Print the values
	echo"<p>The book <em>$book</em> was written by $first_name $last_name.</p>";
	?>
</body>
</html>